package net.watchnow.bean;

import java.io.Serializable;
import java.util.Date;

public class TestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7857621750111523953L;

	private String id;
    private String userName;
    private String passWord;
    private Date time;
    private int num;
    private Object object;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
    
}
