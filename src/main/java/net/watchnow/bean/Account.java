package net.watchnow.bean;

import java.io.Serializable;
import java.util.Date;

public class Account implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8660772406808478670L;

	private String id;
	
	private String fullName;
	
	private String userName;
	
	private String pwd;
	
	private String email;
	
	private boolean globalAdmin;
	
	private Date createTime;
	
	private Date updateTime;
	
	private String apiKey;
	
	private int offer; // 登录次数
	
	private Date lastLogin;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isGlobalAdmin() {
		return globalAdmin;
	}

	public void setGlobalAdmin(boolean globalAdmin) {
		this.globalAdmin = globalAdmin;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public int getOffer() {
		return offer;
	}

	public void setOffer(int offer) {
		this.offer = offer;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	
	
	
}
