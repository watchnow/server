package watchnow.test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.watchnow.Application;
import net.watchnow.bean.TestBean;
import net.watchnow.dao.TestBeanDao;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=Application.class)// 指定spring-boot的启动类 
public class AppTest {

	@Autowired
	private TestBeanDao dao;
	
	@Test
	public void TestBeanRmAndAdd() {
		String name = "alala";
		dao.deleteOneByName(name);
		TestBean bean = new TestBean();
		bean.setId(UUID.randomUUID().toString());
		bean.setNum((int)(Math.random() * 1000));
		bean.setTime(new Date());
		bean.setUserName(name);
		bean.setPassWord("123456");
		Map<String, Object> test = new HashMap<String, Object>();
		test.put("nums", 123);
		test.put("oo", "123456");
		test.put("doubls", 123.123);
		bean.setObject(test);
		dao.saveOne(bean);
		
	}
	
}
