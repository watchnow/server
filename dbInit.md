## 首先安装mongodb, 登录到后台

## 创建watchnow库
执行如下语句
```
use watchnow
```

## 创建用户
执行
```
db.createUser({user:"watch", pwd: "watch", roles:[{role:"readWrite", db: "watchnow"}]})
```

## 使用户生效
```
db.auth("watch","watch")
```