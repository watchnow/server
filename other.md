## Countly请求内容分析：

### begin:
请求内容如下：
```
begin_session:1
metrics:{"_app_version":"v2.9.2","_ua":"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36","_resolution":"1920x1080","_density":1,"_locale":"zh-CN"}
app_key:57c4b2f52381165d8d29d5d7a2cec5dce197658f
device_id:e0:d5:5e:64:00:50
sdk_name:javascript_native_web
sdk_version:18.04
country_code:CN
city:新疆
ip_address:10.100.102.250
timestamp:1530611606975
hour:17
dow:2
```

### url event:
```
events:[{"key":"[CLY]_view","count":1,"segmentation":{"name":"/outpatientWorkstation.html","visit":1,"domain":"localhost"},"timestamp":1530611606976,"hour":17,"dow":2}]
app_key:57c4b2f52381165d8d29d5d7a2cec5dce197658f
device_id:e0:d5:5e:64:00:50
sdk_name:javascript_native_web
sdk_version:18.04
country_code:CN
city:新疆
ip_address:10.100.102.250
timestamp:1530611606977
hour:17
dow:2
```

### session:
```
session_duration:61
app_key:57c4b2f52381165d8d29d5d7a2cec5dce197658f
device_id:e0:d5:5e:64:00:50
sdk_name:javascript_native_web
sdk_version:18.04
country_code:CN
city:新疆
ip_address:10.100.102.250
timestamp:1530611667248
hour:17
dow:2
```


## 直接获取本机ip相关信息接口：
http://ip.chinaz.com/getip.aspx

## app文档结构
```
{
    "_id": "5b064d53465f327819f2bd84",
    "name": "HIS前端",
    "country": "CN",
    "type": "web",
    "category": "6",
    "timezone": "Asia/Shanghai",
    "app_domain": "",
    "created_at": 1527139667,
    "edited_at": 1527139667,
    "owner": "5b064bd4a4f19d0012d8423c",
    "seq": 42,
    "key": "57c4b2f52381165d8d29d5d7a2cec5dce197658f",
    "sdk_version": "18.04"
}
```

## 用户
```
{
    "_id": "5b064bd4a4f19d0012d8423c",
    "full_name": "孙军",
    "username": "sjun1990",
    "password": "8ec0e88510e4feca0c9667d40c0ef95ac2117ec574ac1e7cc3e9c564c0c29708042339b763195a32e05c767f718afdc980b6b933621887dd2fe1b0c6fb8bde88",
    "email": "xjwssjun@qq.com",
    "global_admin": true,
    "created_at": 1527139284,
    "password_changed": 1527139284,
    "lang": "zh",
    "api_key": "ecb0d1a8b9cb7d21b8edaa110c13dea7",
    "in_user_id": "7b78ec46e8af83694ec41e77e8012970",
    "in_user_hash": "ba3581857aaef33704f5c70a25c36d4d0185d5fdfbc19163380c17aee0606bee",
    "offer": 2,
    "active_app_id": "5b064d53465f327819f2bd84",
    "last_login": 1530692495
}
```